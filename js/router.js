var app = angular.module('app', ['ui.bootstrap', 'ngRoute', 'angularMoment', 'wt.responsive', 'ngMap', 'cfp.hotkeys', 'ngAnimate', 'ngSanitize', 'ui.select', 'ui.bootstrap.datetimepicker', 'angularjs-dropdown-multiselect', 'ngIdle'])
.config(function ($routeProvider, $locationProvider, IdleProvider, KeepaliveProvider) {
    $routeProvider.caseInsensitiveMatch = false;
    $routeProvider
            .when('/', {
                templateUrl: 'page/login.html',
                controller: 'LoginController'
            })
            .when('/login', {
                templateUrl: 'page/login.html',
                controller: 'LoginController'
            })
            .when('/role', {
                templateUrl: 'page/role.html',
                controller: 'RoleController',
                resolve: {
                    roles_asset: function ($http, userSession, path) {
                        return $http.get(path.api + 'module=user&action=get_roles_asset&ad_user_id=' + userSession.get('ad_user_id')).then(function (response) {
                            if (response.data.error === false) {
                                return response.data.data;
                            } else {
                                return [];
                            }
                        }, function (error) {
                            //predefined.showToast(error.statusText, 'info', true, 3000, true);
                            return [];
                        });
                    }
                }
            })
            .when('/dashboard', {
                templateUrl: 'page/dashboard.html',
                controller: 'DashboardController'
            })
            .when('/onlineorder', {
                templateUrl: 'page/onlineorder.html',
                controller: 'OnlineOrderController'
            })
            .when('/getstarted', {
                templateUrl: 'page/getstarted.html',
                controller: 'GetStartedController'
            })
            .when('/setuporg', {
                templateUrl: 'page/setuporg.html',
                controller: 'SetOrgController'
            })
            .when('/menulist', {
                templateUrl: 'page/menulist.html',
                controller: 'MenuListController'
            })
            .when('/shipper', {
                templateUrl: 'page/shipper.html',
                controller: 'ShipperController'
            })
            .when('/shipperdetail/:shid', {
                templateUrl: 'page/shipperdetail.html',
                controller: 'ShipperDetailController' 
            })
            .when('/freight', {
                templateUrl: 'page/freight.html',
                controller: 'FreightController'
            })
            .when('/home', {
                templateUrl: 'page/home.html',
                controller: 'HomeController'
            })
            .when('/customer', {
                templateUrl: 'page/user.html',
                controller: 'UserController'
            })
            .when('/subscriber', {
                templateUrl: 'page/user.html',
                controller: 'UserController'
            })
            .when('/invoice/:type', {
                templateUrl: 'page/invoice.html',
                controller: 'InvoiceController'
            })
            .when('/product_category', {
                templateUrl: 'page/productcategory.html',
                controller: 'ProductCategoryController'
            })
            .when('/productcategorydetail/:pcid', {
                templateUrl: 'page/productcategorydetail.html',
                controller: 'ProductCategoryDetailController'
            })
            .when('/product_group', {
                templateUrl: 'page/productgroup.html',
                controller: 'ProductGroupController'
            })
            .when('/productgroupdetail/:pgid', {
                templateUrl: 'page/productgroupdetail.html',
                controller: 'ProductGroupDetailController'
            })
            .when('/order', {
                templateUrl: 'page/order.html',
                controller: 'OrderController'
            })
            .when('/orderdetail/:oid', {
                templateUrl: 'page/orderdetail.html',
                controller: 'OrderDetailController'
            })
            .when('/color', {
                templateUrl: 'page/color.html',
                controller: 'ColorController'
            })
            .when('/colordetail/:pclid', {
                templateUrl: 'page/colordetail.html',
                controller: 'ColorDetailController'
            })
           .when('/size', {
                templateUrl: 'page/size.html',
                controller: 'SizeController'
            })
            .when('/sizedetail/:psid', {
                templateUrl: 'page/sizedetail.html',
                controller: 'SizeDetailController'
            })
            .when('/general_ledger', {
                templateUrl: 'page/generalledger.html',
                controller: 'GeneralLedgerController'
            })
            .when('/general_ledger_detail/:id', {
                templateUrl: 'page/generalledgerdetail.html',
                controller: 'GeneralLedgerDetailController'
            })
            .when('/purchase_invoice', {
                templateUrl: 'page/purchaseinvoice.html',
                controller: 'PurchaseInvoiceController'
            })
            .when('/purchase_invoice_detail/:id', {
                templateUrl: 'page/purchaseinvoicedetail.html',
                controller: 'PurchaseInvoiceDetailController'
            })
            .when('/devicemanager', {
                templateUrl: 'page/devicemanager.html',
                controller: 'DeviceManagerController'
            })
            .when('/devicemanagerdetail/:id', {
                templateUrl: 'page/devicemanagerdetail.html',
                controller: 'DeviceManagerDetailController'
            })
            .when('/warehouse', {
                templateUrl: 'page/warehouse.html',
                controller: 'WarehouseController'
            })
            .when('/warehousedetail/:id', {
                templateUrl: 'page/warehousedetail.html',
                controller: 'WarehouseDetailController'
            })
            .when('/uom', {
                templateUrl: 'page/uom.html',
                controller: 'UomController'
            })
            .when('/uomdetail/:uid', {
                templateUrl: 'page/uomdetail.html',
                controller: 'UomDetailController'
            })
            .when('/product', {
                templateUrl: 'page/product.html',
                controller: 'ProductController'
            })
            .when('/productdetail/:pid', {
                templateUrl: 'page/productdetail.html',
                controller: 'ProductDetailController' 
            })
            .when('/vendor', {
                templateUrl: 'page/vendor.html',
                controller: 'VendorController'
            })
            .when('/vendordetail/:pid', {
                templateUrl: 'page/vendordetail.html',
                controller: 'VendorDetailController'
            })
            .when('/reportcategory', {
                templateUrl: 'page/reportcategory.html',
                controller: 'ReportCategoryController'
            })
            .when('/reportlist/:cat', {
                templateUrl: 'page/reportlist.html',
                controller: 'ReportListController'
            })
            .when('/reportcontainer/:cat/:type', {
                templateUrl: 'page/reportcontainer.html',
                controller: 'ReportContainerController'
            })
            .otherwise({
                redirectTo: '/login'
            });
		    IdleProvider.idle(600); // in seconds
		    IdleProvider.timeout(5); // in seconds
		    KeepaliveProvider.interval(5); // in seconds

            if (window.history && window.history.pushState) {
                //$locationProvider.html5Mode(true); will cause an error $location in HTML5 mode requires a  tag to be present! Unless you set baseUrl tag after head tag like so: <head> <base href="/">
                // to know more about setting base URL visit: https://docs.angularjs.org/error/$location/nobase
                // if you don't wish to set base URL then use this
                $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                });
            }

}).run(function ($rootScope, $location, userSession) {
    $rootScope.isLoading = false;
    $rootScope.enableRootMenu = false;
    $rootScope.breadcrumb_url = 'Dashboard';
    $rootScope.selectedmenu = '';
    $rootScope.previousRecord = {window: '', page: 1, index: 0, search: '', filter:{}, filterData:{}};
    $rootScope.curr_date_time = new Date();
    $rootScope.pusher = new Pusher('dab2dc5a6ec0598a0f1a', {
        cluster: 'ap2'
    });

    $rootScope.app_id = "1010757";
    $rootScope.key = "6072ef6d9e6cce25aaa7";
    $rootScope.secret = "ba9263a4b52f6b8205c9";
    $rootScope.cluster = "ap2";


    if (userSession.get('ad_user_id') === null) {
        $location.path('login');
    }
    $rootScope.isloginpage = false;
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (userSession.get('ad_user_id') === null) {
            $location.path('login');
        }
        if (typeof next === 'undefined' || typeof next.$$route === 'undefined' || next.$$route.controller === 'LoginController' || next.$$route.controller === 'RoleController') {
            $rootScope.isloginpage = true;
        } else {
            $rootScope.isloginpage = false;
        }
    });
    $rootScope.$on('IdleTimeout', function () {
    	userSession.removeAll();
        location.reload();
    });
});
