app.controller('HomeController', function ($uibModal, $scope, hotkeys) {
	$scope.expandmenu = false;
	$scope.openDialog = function () {
		var modalInstance = $uibModal.open({
			animation: true,
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'model/calendar.html',
			controller: ModalController,
			size: 'lg',
			windowClass: 'full-width-modal',
			resolve: {
				data: function () {
					return null;
				}
			}
		});
		modalInstance.result.then(function () {
			console.log('Closed');
		}, function () {
			console.log('Dismissed');
		});
	};
	//hotkeys.del('f10');
	hotkeys.bindTo($scope).add({
		combo: 'f12',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Print');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f2',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('New');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f6',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Search');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f3',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Delete');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f7',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Attachment');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f8',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Layout');
			event.preventDefault();
		}
	});
	hotkeys.bindTo($scope).add({
		combo: 'f9',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			alert('Filter');
			event.preventDefault();
		}
	});
});

function ModalController($scope, $uibModalInstance) {
	$scope.ok = function () {
		$uibModalInstance.close();
	};
	$scope.cancel = function () {
		$uibModalInstance.dismiss();
	};
	$scope.calendar = {year: moment().format('YYYY'), month: '01', start_date: 1, end_date: moment(this.year + '-' + this.month + '-01').endOf('month').format('DD')};
	$scope.getYears = function () {
		var array = [];
		for (var i = 2020; i > 1991; i--) {
			array.push(i);
		}
		return array;
	};
	$scope.changeCalendar = function () {
		$scope.calendar.end_date = moment($scope.calendar.year + '-' + $scope.calendar.month + '-01').endOf('month').format('DD');
	};
	$scope.getDays = function () {
		var array = [];
		for (var i = 1; i <= $scope.calendar.end_date; i++) {
			array.push(i);
		}
		return array;
	};
}

function BarcodeController($scope, $uibModalInstance, userSession, $http, path, $window, hotkeys, $timeout) {
	$timeout(function () {
		$window.document.getElementById('product_qty').focus();
	}, 200);
	$scope.ok = function () {
		$uibModalInstance.close();
	};
	$scope.cancel = function () {
		$uibModalInstance.dismiss();
	};
	//$scope.params = product;
	$scope.params.product_qty = 1;
	hotkeys.bindTo($scope).add({
		combo: 'f12',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			$scope.printBarcode();
			event.preventDefault();
		}
	});

	$scope.printBarcode = function () {
		var data = {};
		data.ad_client_id = userSession.get('ad_client_id');
		data.ad_org_id = userSession.get('selected_org_id');
		data.ad_user_id = userSession.get('ad_user_id');
		data.mProductModel = {m_product_id: $scope.params.pid, quantity: $scope.params.product_qty};
		data.serviceType = 'PRINT_PRODUCT_BARCODE';
		data.isShowPrice = 'N';
		if ($scope.isShowPrice) {
			data.isShowPrice = 'Y';
		}
		$http.post(path.apitomcat + 'Print', data).then(function (res) {
			if (res.data.isError === true)
				return;
			res = res["data"];
			window.open(res["data"]["report_base_url"]["value"] + '/' + res["data"]["report_base_url"]["ad_client_id"] + '/' + res["data"]["report_base_url"]["ad_org_id"] + '/' + res["data"]["report_base_url"]["ad_table_id"] + '/' + res["data"]["print_report_model"]["fileName"], '_blank');
		});
	};
}

function ImageAttachmentController($scope, $rootScope, $uibModalInstance, userSession, $http, path, $window, hotkeys, $timeout) {
	$scope.curr_img_index = 0;

	$scope.ok = function () {
		$uibModalInstance.close();
	};
	$scope.cancel = function () {
		$uibModalInstance.dismiss();
	};
	
	hotkeys.bindTo($scope).add({
		combo: 'alt+a',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			$scope.removeAttachment();
			event.preventDefault();
		}
	});

	hotkeys.bindTo($scope).add({
		combo: 'alt+a',
		allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
		callback: function (event) {
			//$scope.printBarcode();
			event.preventDefault();
		}
	});


	$scope.navigateImage = function(index) {
		if( typeof index == undefined || index == '' || $scope.params.images == undefined || $scope.params.images == '' )
			return;

		var tempIndex = (($scope.curr_img_index)+(index));
		if( tempIndex >= 0 && tempIndex <= $scope.params.images.length-1 )
			$scope.curr_img_index = tempIndex;
	};

	$scope.fileOnChange = function(e) {
		var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
		var pattern = /image-*/;
		var reader = new FileReader();
		if (!file.type.match(pattern)) {
			this._snackBar.open('invalid format', '', {
				duration: 2000,
			});
			return;
		}
		$scope.image_name = file.name;
		reader.onload = $scope.handleReaderLoaded.bind(this);
		reader.readAsDataURL(file);
	}

	$scope.handleReaderLoaded = function(e) {
		let reader = e.target;
		var avatar_base64 = (reader.result).replace("data:image/jpeg;base64,", "");
		avatar_base64 = (avatar_base64).replace("data:image/png;base64,", "");
		$scope.binarydata = avatar_base64;

		//uploading
		$scope.addAttachment();
	}

	$scope.addAttachment = function () {
		var data = {};
		data.ad_client_id = userSession.get('ad_client_id');
		data.ad_org_id = userSession.get('selected_org_id');
		data.ad_user_id = userSession.get('ad_user_id');
		data.m_product_id = $scope.params.pid;
		data.imageModel = {name: $scope.image_name, binarydata: $scope.binarydata};
		data.serviceType = 'ADD_PRODUCT_IMAGE';
		$rootScope.isLoading = true;
		$http.post(path.apitomcat + 'Product', data).then(function (res) {
			$rootScope.isLoading = false;
			if (res.data.isError === true)
				return;
			res = res["data"];
			if( typeof res.data.images !=undefined && res.data.images != '' )
				$scope.params.images = res.data.images;
			alert(res.successMsg);
		}, function (error) {
			$rootScope.isLoading = false;
		});
	};

	$scope.removeAttachment = function(){
		var imgArr = $scope.params.images;
		var image_url = $scope.params.images[$scope.curr_img_index];

		var data = {};
		data.ad_client_id = userSession.get('ad_client_id');
		data.ad_org_id = userSession.get('selected_org_id');
		data.ad_user_id = userSession.get('ad_user_id');
		data.record_id = $scope.params.pid;
		data.name = image_url.substr(image_url.lastIndexOf("/")+1);
		data.serviceType = 'REMOVE_ATTACHMENT_BY_NAME';
		$rootScope.isLoading = true;
		$http.post(path.apitomcat + 'Image', data).then(function (res) {
			$rootScope.isLoading = false;
			if (res.data.isError === true)
				return;
			res = res["data"];

			imgArr.splice($scope.curr_img_index,1);
			$scope.params.images = imgArr; 
			//if( $scope.curr_img_index > 0 )
				$scope.curr_img_index=0;
			
			alert(res.successMsg);
		}, function (error) {
			$rootScope.isLoading = false;
		});
	};

}

function AlertController($scope, $uibModalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$uibModalInstance.close();
	};
	$scope.cancel = function () {
		$uibModalInstance.dismiss();
	};
}

function SearchMenuModalInstanceCtrls($scope, $uibModalInstance, items, $http, path,$rootScope ,$uibModal, $timeout, $location, userSession, hotkeys) {
    $scope.selected = {url:{}};
    $scope.selected.url.name = "Search Menu";
    $scope.baseUrls = [
        {name:'Dashboard',value:'dashboard'},
        {name:'Online Order',value:'onlineorder'},
        {name:'Customer',value:'customer'},
        {name:'Product',value:'product'},
        {name:'Product Category',value:'product_category'},
        {name:'Product Group',value:'product_group'},
        {name:'Vendor',value:'vendor'},
        {name:'Color',value:'color'},
        {name:'Size',value:'size'},
        {name:'UOM',value:'uom'},
        {name:'Order',value:'order'},
        {name:'Sales Invoice',value:'invoice/sales'},
        {name:'Purchase Invoice',value:'purchase_invoice'},
        {name:'General Ledger',value:'general_ledger'},
        {name:'Report',value:'reportcategory'},
        {name:'Setup Organization',value:'setuporg'}
        
    ];
//    $timeout(function () {
//        $('#url_dropdown').find('span')[0].click();
//   });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.triggerMenuChange = function () {
        if( $scope.selected == undefined || $scope.selected.url == undefined )
            return;
        $scope.cancel();
        $scope.navigate( $scope.selected.url.value );
    };
    $scope.ok = function(){
        $scope.cancel();
        $scope.navigate( $scope.selected.url.value );
    }
    
    $scope.navigate = function ( path ) {
        $location.path( path );
    };
    
	hotkeys.bindTo($scope).add({
        combo: 'esc',
        allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
        callback: function (event) {
            $scope.cancel();
        }
    });
}

function uploadPopupOpenModalInstanceCtrls($scope, $uibModalInstance, items, $http, path,$rootScope ,$uibModal,userSession, hotkeys, $window, $location ) {
	$scope.data = {};
	$scope.selected_index = 0;
	$scope.fileTypes = new Map();
	$scope.fileTypes.set('excel', 'excel.svg');
	$scope.fileTypes.set('pdf', 'pdf.svg');
	$scope.fileTypes.set('image', 'photo.svg');
	$scope.fileTypes.set('other', 'photo.svg');
	
	$scope.getImageByExtension = function (type) {
		var image = ['png', 'jpg'];
		var document = ['doc', 'docx', 'txt', 'pdf', 'rtf'];
		var excel = ['xlsx', 'xls', 'csv'];
		
		 if (image.indexOf(type) !== -1) {
			 return $scope.fileTypes.get('image'); 
		 } else if (document.indexOf(type) !== -1) {
			 return $scope.fileTypes.get('pdf');
		 } else if (excel.indexOf(type) !== -1) {
			 return $scope.fileTypes.get('excel');
		 } else {
			 return $scope.fileTypes.get('other');
		 }
	};
	
	$scope.getImages = function () {
		var formData = {
				ad_client_id: userSession.get('ad_client_id'),
				ad_org_id: userSession.get('selected_org_id'),
				ad_user_id : userSession.get('ad_user_id'),
				ad_table_id: $scope.params.ad_table_id,
				record_id: $scope.params.record_id
			};
			$http.post(path.apiLara + '/portal/attachment/getAll', formData).then(function (response) {
				$scope.data = response.data;
				console.log($scope.data);
				$rootScope.isLoading = false;
			}, function (error) {
				console.log('error');
				$rootScope.isLoading = false;
			});
	};
	
	$scope.fileOnChange = function(e) {
		var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
		if (file == undefined || file == '') { 
		    alert('Invalid file type');
		    return;
		} 
		$rootScope.isLoading = true;
		var formData = new FormData();
        formData.append('ad_client_id', userSession.get('selected_client_id'));
        formData.append('ad_org_id', userSession.get('selected_org_id'));
        formData.append('ad_user_id', userSession.get('ad_user_id'));
        formData.append('record_id', $scope.params.record_id);
        formData.append('ad_table_id', $scope.params.ad_table_id);
        formData.append('ad_attachment_id', $scope.data.modelDetail.ad_attachment_id);
        formData.append('file' , file);
		$http.post(path.apiLara + '/portal/attachment/add', formData, {transformRequest: angular.identity, headers: {'content-Type': undefined}}).then(function (response) {
			$rootScope.isLoading = false;
			console.log(response.data);
			if( response.data.success  ) {
				document.getElementById('attachment-upload').value = '';
				$scope.data = response.data;
			}
			alert( response.data.message );
		}, function (error) {
			$rootScope.isLoading = false;
		});
	};
	
	$scope.remove = function(file) {
		var formData = new FormData();
        formData.append('ad_client_id', userSession.get('selected_client_id'));
        formData.append('ad_org_id', userSession.get('selected_org_id'));
        formData.append('ad_user_id', userSession.get('ad_user_id'));
        formData.append('record_id', $scope.params.record_id);
        formData.append('ad_table_id', $scope.params.ad_table_id);
        formData.append('ad_attachment_id', $scope.data.modelDetail.ad_attachment_id);
        formData.append('file' , file);
		$rootScope.isLoading = true;
		$http.post(path.apiLara + '/portal/attachment/remove', formData, {transformRequest: angular.identity, headers: {'content-Type': undefined}}).then(function (response) {
			$rootScope.isLoading = false;
			console.log(response.data);
			if( response.data.success  ) {
				$scope.data = response.data;
			}
			alert( response.data.message );
		}, function (error) {
			$rootScope.isLoading = false;
		});
	};
	
	$scope.navigateToNew = function (url) {
		window.open( url, '_blank');
	};
	
	
    $scope.selectRow = function (ind) {
        $scope.selected_index = ind;
    };
    
    $scope.getRecordDetail = function (ind) {
    	$window.document.getElementById('detail_'+ ind).click();
    };

	if( $scope.params != undefined && $scope.params.images == undefined || $scope.params.images.length == 0 ) {
		$scope.getImages();
	}
   
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    
    hotkeys.bindTo($scope).add({
        combo: 'esc',
        allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
        callback: function (event) {
            $scope.cancel();
        }
    });
    hotkeys.bindTo($scope).add({
        combo: 'alt+u',
        allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
        callback: function (event) {
        	$window.document.getElementById('attachment-upload').click();
        }
    });
    hotkeys.bindTo($scope).add({
        combo: 'enter',
        callback: function (event) {
    		$scope.getRecordDetail($scope.selected_index);
    		event.preventDefault();
        }
    });
    hotkeys.bindTo($scope).add({
        combo: 'down',
		callback: function (event) {
			if ( $scope.data.modelDetail != undefined && $scope.data.modelDetail.files != undefined
					&& $scope.selected_index < $scope.data.modelDetail.files.length-1 ) {
				$scope.selected_index = $scope.selected_index + 1;
			}
			event.preventDefault();
		}
    });
    hotkeys.bindTo($scope).add({
        combo: 'up',
        callback: function (event) {
			if ($scope.selected_index > 0) {
				$scope.selected_index = $scope.selected_index - 1;
			}
            event.preventDefault();
        }
    });

}