app.controller('LoginController', function ($scope, $http, $location, userSession, path, $window, hotkeys, shortcut, $rootScope, Idle) {
    shortcut.deleteAll();
    $scope.form = {};
    $scope.loading = false;
    $scope.err_msg = '';
    $scope.submitLogin = function () {
        $scope.err_msg = '';
        var formData = new FormData();
        formData.append('name', $scope.form.name);
        formData.append('password', $scope.form.password);
        $scope.loading = true;
        $http.post(path.api + 'module=user&action=login', formData, {transformRequest: angular.identity, headers: {'content-Type': undefined}}).then(function (response) {
            $scope.loading = false;
            if (response.data.error === false) {
                angular.forEach(response.data.data, function (val, key) {
                    userSession.set(key, val);
                });
                Idle.watch();
                $location.path('role');
            } else {
        		$scope.err_msg = (response.data.message != undefined && response.data.message != '')?response.data.message:'Invalid login details';
            }
        }, function (error) {
            $scope.loading = false;
            $scope.err_msg = error.statusText;
        });
    };
    $window.document.getElementById('username').focus();
});