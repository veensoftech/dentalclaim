/* app.service('path', function (userSession) {
    var paths = {api: 'api/api.php?', apitomcat: 'http://192.168.1.81:8080/QSweb/Api/',
        //apiLara: 'http://api.qsale.qa',
        apiLara: 'http://api.qsale.localhost:88',
        report_excel_url: 'http://demo.empower-erp.com:90/webui',
        basic: ''};
    return paths;
}); */

//production-1
//app.service('path', function (userSession) {
//var paths = {api: 'api/api.php?', apitomcat: 'http://empower-erp.com:8080/QSweb/Api/',
//        apiLara: 'http://empower-erp.com:81/empower-erp/public',
//        //apiLara: 'http://api.qsale.localhost:88',
//        basic: ''};
//	return paths;
//});

//production-2
//app.service('path', function (userSession) {
//    var paths = {api: 'api/api.php?', apitomcat: 'http://qsale.qa:8080/QSweb/Api/',
//        apiLara: 'http://api.qsale.qa',
//        // apiLara: 'http://api.qsale.localhost',
//        basic: ''};
//    return paths;
//});

//demo
app.service('path', function (userSession) {
    var paths = {api: 'api/api.php?', apitomcat: 'https://demo.empower-erp.com:8543/QSweb/Api/',
        apiLara: 'https://demo.empower-erp.com:457/empower-erp/public',
        // apiLara: 'http://api.qsale.localhost',
        report_excel_url: 'http://demo.empower-erp.com:90/webui',
        basic: ''};
    return paths;
});

app.service('predefined', function ($uibModal) {
    return {
        showAlert: function (ttl, msg, icon) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'model/alert.html',
                controller: AlertController,
                size: 'md',
                windowClass: 'radius-10',
                resolve: {
                    data: function () {
                        return {title: ttl, message: msg, icon: icon};
                    }
                }
            });
            modalInstance.result.then(function () {
                console.log('Closed');
            }, function () {
                console.log('Dismissed');
            });
        }
    };
});

app.service('userSession', function () {
    var setValue = function (key, val) {
        window.sessionStorage.setItem('zearo_' + key, val);
    };
    var getValue = function (key) {
        return window.sessionStorage.getItem('zearo_' + key);
    };
    var removeValue = function (key) {
        return window.sessionStorage.removeItem('zearo_' + key);
    };
    var removeAll = function () {
        return window.sessionStorage.clear();
    };
    return {
        set: setValue, get: getValue, remove: removeValue, removeAll: removeAll
    };
});

app.service('shortcut', function (hotkeys) {
    return {
        deleteAll: function () {
            hotkeys.del('f5');
            hotkeys.del('f6');
            hotkeys.del('g+d');
            hotkeys.del('g+u');
            hotkeys.del('g+r');
            hotkeys.del('g+p');
            hotkeys.del('g+l');
            hotkeys.del('alt+t');
        }
    };
});
