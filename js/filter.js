app.filter('timeformat', function () {
    return function (input) {
        if (typeof input !== 'undefined') {
            var time = input.split(':');
            return moment().set({hour: time[0], minute: time[1], second: time[2]}).format('hh:mm A');
        } else {
            return input;
        }
    };
});

app.filter('dateformat', function () {
    return function (input) {
        if (typeof input !== 'undefined') {
            return moment(input).format('DD-MM-YYYY');
        } else {
            return input;
        }
    };
});

app.filter('numberFormat', function () {
    return function (input) {
            return Number(input);
    };
});

app.filter('padding', function () {
    return function (input) {
        if (input) {
            var res = input;
            if (parseInt(input) < 10) {
                res = '0' + input;
            }
            return res;
        } else {
            return '-';
        }
    };
});

app.filter('paddingspace', function () {
    return function (input, count) {
        if (input) {
            var res = '';
            res = ((input).toString()).padStart(count, ' ');
            return res;
        } else {
            return '-';
        }
    };
});

app.filter('grandTotal', function () {
    return function (input, type) {
        if (input) {
            var res = 0;
            switch (type) {
                case 'dashboard_sale':
                    angular.forEach(input, function (val, key) {
                        if(key != undefined && key == 'Sale return (Cash)')
                    		return;
                        res = parseFloat(res) + parseFloat(val);
                    });
                    break;
                case 'accounts_total':
                    angular.forEach(input, function (val, key) {
                        res = parseFloat(res) + parseFloat(val.total_amount);
                    });
                    break;
                case 'invoice_total':
                    angular.forEach(input, function (val, key) {
                        res = parseFloat(res) + parseFloat(val.grandtotal);
                    });
                    break;
                default:
                    break;
            }
            return res;
        } else {
            return 0;
        }
    };
});

app.filter('trustAsResourceUrl', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsResourceUrl(val + '#toolbar=0');
        };
    }]);

app.filter('firstchar', function () {
    return function (input) {
        if (input) {
            return input.charAt(0).toUpperCase();
        } else {
            return 'Z';
        }
    };
});

app.filter('charlimit', function () {
    return function (input, len) {
        if (typeof input !== 'undefined' && input !== null && input !== '') {
            if (input.length <= len) {
                return input;
            } else {
                return input.substr(0, (len - 3)) + '...';
            }
        }
    };
});
