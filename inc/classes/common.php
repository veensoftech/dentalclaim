<?php

class Common {

    public function selectAllWithoutWhere($table) {
        global $conn;
        $data = array();
        $result = mysqli_query($conn, 'SELECT * FROM ' . $table . '');
        oci_execute($result);
        while ($row = fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    public function selectRowWithoutWhere($table) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, 'SELECT * FROM `' . $table . '`');
        if (pg_num_rows($result) > 0) {
            $data = fetch_assoc($result);
        }
        return $data;
    }

    public function selectAllWithWhere($table, $where) {
        $data = array();
        global $conn;
        $result = $conn->query($conn, 'SELECT * FROM ' . $table . ' WHERE ' . $where);
        if (num_rows($result) > 0) {
            while ($row = fetch_assoc($result)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function selectRowWithWhere($table, $where) {
        $data = array();
        global $conn;
        $result = $conn->query('SELECT * FROM ' . $table . ' WHERE ' . $where);
        if ($result->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $data = $row;
            }
        }
        return $data;
    }

    public function selectAllCustomWithoutWhere($fields, $table) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, 'SELECT ' . implode(",", $fields) . ' FROM `' . $table . '`');
        if (pg_num_rows($result) > 0) {
            while ($row = fetch_assoc($result)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function executeRow($qry) {
        global $conn;
        $data = array();
        $result = mysqli_query($conn, $qry);
        if (pg_num_rows($result) > 0) {
            $data = fetch_assoc($result);
        }
        return $data;
    }

    public function executeAllParams($qry, $empty = null) {
        global $conn;
        $data = array(array('key' => $empty, 'name' => '-'));
        $result = mysqli_query($conn, $qry);
        if (pg_num_rows($result) > 0) {
            while ($row = fetch_assoc($result)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function selectRowCustomWithoutWhere($fields, $table) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, 'SELECT ' . implode(",", $fields) . ' FROM ' . $table . '');
        if (pg_num_rows($result) > 0) {
            $data = fetch_assoc($result);
        }
        return $data;
    }

    public function selectAllCustomWithWhere($fields, $table, $where) {
        global $conn;
        $data = array();
        $result = mysqli_query($conn, 'SELECT ' . implode(",", $fields) . ' FROM ' . $table . ' WHERE ' . $where);
        while ($row = fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    public function selectAllCustomWithWhere2($fields, $table, $where) {
        global $conn;
        $data = array();
        $result = mysqli_query($conn, 'SELECT ' . $fields . ' FROM ' . $table . ' WHERE ' . $where);
        while ($row = fetch_assoc($result)) {
            $data[] = $row;
        }
        return $data;
    }

    public function selectRowCustomWithWhere($fields, $table, $where) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, 'SELECT ' . implode(",", $fields) . ' FROM ' . $table . ' WHERE ' . $where);
        if (pg_num_rows($result) > 0) {
            $data = fetch_assoc($result);
        }
        return $data;
    }

    public function executeRowmysqli_query($qry) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, $qry);
        if (pg_num_rows($result) > 0) {
            $data = fetch_assoc($result);
        }
        return $data;
    }

    public function executeAll($qry) {
        global $conn;
        $data = array();
        $result = mysqli_query($conn, $qry);
        if (pg_num_rows($result) > 0) {
            while ($row = fetch_assoc($result)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function executeAllmysqli_query($qry) {
        $data = array();
        global $conn;
        $result = mysqli_query($conn, $qry);
        if (pg_num_rows($result) > 0) {
            while ($row = fetch_assoc($result)) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function escapeString($val) {
        return pg_escape_string($val);
    }

    public function insertRecord($fields, $table) {
        $values = array_values($fields);
        global $conn;
        $result = mysqli_query($conn, 'INSERT INTO ' . $table . ' (' . implode(", ", array_keys($fields)) . ') VALUES (\'' . implode("','", array_map(array($this, 'escapeString'), $values)) . '\')');
        if (pg_affected_rows($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updateRecordWithWhere($fields, $table, $where) {
        $records = [];
        global $conn;
        foreach ($fields as $key => $val) {
            array_push($records, $key . ' = \'' . pg_escape_string($conn, $val) . '\'');
        }
        $result = mysqli_query($conn, 'UPDATE ' . $table . ' SET ' . implode(',', $records) . ' WHERE ' . $where);
        return pg_affected_rows($result);
    }

    public function getReplacedString($str, $replaces) {
        foreach ($replaces as $key => $val) {
            $str = str_replace('[' . $key . ']', $val, $str);
        }
        return $str;
    }

    public function deleteRecordWithWhere($table, $where) {
        global $conn;
        $result = mysqli_query($conn, 'DELETE FROM ' . $table . ' WHERE ' . $where);
        return pg_affected_rows($result);
    }

    public function saveFile($file, $path) {
        $name = $file['name'];
        $temp_name = $file['tmp_name'];
        $extension = strtolower($this->getExtension($name));
        $key = md5(uniqid());
        $temp_file_name = $key . '.' . $extension;
        $dir = $path;
        if (!file_exists($dir)) {
            mkdir($dir);
        }
        $temp_file_path = $path . '/' . $name;
        move_uploaded_file($temp_name, $temp_file_path);
        return $temp_file_path;
    }

    public function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    public function removeSplChars($str) {
        $string = str_replace(' ', '-', $str); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

}
