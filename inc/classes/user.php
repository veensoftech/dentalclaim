<?php

class User {

    public function getTasks($c_bpartner_id, $ad_client_id, $ad_org_id) {
        global $conn;
        $data = array();
        //$org_where = ' AND pt.ad_org_id=' . $ad_org_id;
        if ($ad_org_id == 0) {
            $org_where = '';
        }
        $result = pg_query($conn, 'SELECT pt.*, p.name AS projectname, ph.name AS phasename, p.c_project_id FROM c_projecttask pt LEFT JOIN c_projectphase ph ON ph.c_projectphase_id = pt.c_projectphase_id LEFT JOIN c_project p ON p.c_project_id = ph.c_project_id WHERE pt.ad_client_id=' . $ad_client_id . ' AND pt.c_bpartner_id=' . $c_bpartner_id);
        $res = array('error' => true, 'message' => 'Unable to get records');
        if (pg_num_rows($result) > 0) {
            while ($row = pg_fetch_assoc($result)) {
                $data[] = $row;
            }
            $res['error'] = false;
            $res['message'] = 'Records found';
            $res['data'] = $data;
        }
        return $res;
    }

    public function getTaskProjects($c_bpartner_id, $ad_client_id, $ad_org_id) {
        global $conn;
        $data = array();
        //$org_where = ' AND pt.ad_org_id=' . $ad_org_id;
        if ($ad_org_id == 0) {
            $org_where = '';
        }
        $result = pg_query($conn, 'SELECT p.name AS projectname, p.c_project_id FROM c_projecttask pt LEFT JOIN c_projectphase ph ON ph.c_projectphase_id = pt.c_projectphase_id LEFT JOIN c_projecttaskemp te ON te.c_projecttask_id = pt.c_projecttask_id LEFT JOIN c_project p ON p.c_project_id = ph.c_project_id WHERE pt.ad_client_id=' . $ad_client_id . ' AND pt.c_bpartner_id=' . $c_bpartner_id . ' OR te.c_bpartner_id=' . $c_bpartner_id . ' GROUP BY 1,2 ORDER BY 1');
        $res = array('error' => true, 'message' => 'Unable to get records');
        if (pg_num_rows($result) > 0) {
            while ($row = pg_fetch_assoc($result)) {
                $data[] = $row;
            }
            $res['error'] = false;
            $res['message'] = 'Records found';
            $res['data'] = $data;
        }
        return $res;
    }

    public function calloutProcedureOrderAccepet($c_order_id, $ad_client_id, $ad_org_id,$ad_user_id) {
        global $conn;
        $data = array();
        $result = pg_query($conn, 'SELECT adempiere.ee_order_qtyreserved('.$ad_client_id.', '.$ad_org_id.', '.$ad_user_id.', '.$c_order_id.')');
        $res = array('error' => true, 'message' => 'Unable to get records');
        if (pg_num_rows($result) > 0) {
            while ($row = pg_fetch_assoc($result)) {
                $data[] = $row;
            }
            $res['error'] = false;
            $res['message'] = 'Records found';
            $res['data'] = $data;
        }
        return $res;
    }

}
