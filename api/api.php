<?php

require_once '../inc/classes/user.php';
require_once '../inc/classes/common.php';
require_once '../inc/connection.php';
//require_once '../vendor/autoload.php';

$module = filter_input(INPUT_GET, 'module');
$action = filter_input(INPUT_GET, 'action');
switch ($module) {
    case 'user':
        switch ($action) {
            case 'login':
                $name = filter_input(INPUT_POST, 'name');
                $password = filter_input(INPUT_POST, 'password');
                $res = array('error' => true, 'message' => 'Invalid login details');
                $obj = new Common();
                $result = $obj->selectRowWithWhere('UserDetails', 'UserName=\'' . $name . '\' AND PASS=\'' . $password . '\' AND status=\'Y\'');
                if (count($result) > 0) {
                    $res['error'] = false;
                    $res['message'] = 'Data fetched successfully';
                    $res['data'] = $result;
                }
                response_json($res);
                break;
            }
    default:
    break;
}

function response_json($res) {
    header('Content-Type: application/json');
    //header("Content-encoding", "deflate");
    ob_start("ob_gzhandler");
    echo json_encode($res);
    ob_end_flush();
    die();
}
